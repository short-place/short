# Short

The main purpose of this is to take a url and shorten it for a perioud of time.


## High Level

### Main use case

```mermaid
sequenceDiagram
    User->>Website: Navigates top level
    Website-->>User: Presents form
    User-->>Website: Enters long url details
    Website-->>User: return short url
```

### Architectural overview

```mermaid
graph TD;
    elixir_release(Elixir Release)-->phoenix(Phoenix server)
    elixir_release-->job(Job Processor)
    phoenix-->static(React Static Website)
    phoenix-->graphQL
    static-->graphQL
    graphQL-->|create new records|ecto
    job-->|cleanup old urls|ecto
    ecto-->postgres
    

```

## Developer setup

There are some assumptions made around the development environment

* [docker](https://docs.docker.com/engine/install/) compatbile environment 
* [docker-compose](https://docs.docker.com/compose/install/) compatbile environment 
* [asdf](https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies)
* [Elixir](https://github.com/asdf-vm/asdf-elixir)
* [Erlang](https://github.com/asdf-vm/asdf-erlang)

#### Add Plugins

```
asdf plugin add erlang
asdf plugin add elixir
asdf plugin add nodejs
```

#### Install


```
asdf install
```

### Getting started

#### Database

You can either use a postgres server you already  have setup and change the configuration in `dev.exs` or bring up the postgres server with `docker-compose up -d`


#### Setup

To run this in development mode you will need to setup some things like the database. Some helper mix tasks have been setup to make this easier.

```
mix setup
```

#### Running

To open up an interactive terminal start it via:

```
iex -S mix phx.server
```


## Developing

### Graphql

Once the server is running you can route to `/api/graphiql` layer to directly test the api.


## Testing


### UI tests

The UI project uses jest for testing, a mix alias has been setup to make this easier to stay in the root level.

```
mix test.ui
```


### Elixir tests

```
mix test
```

### Performance testing

To run the performance testing locally you need to have [k6](https://k6.io/docs/getting-started/installation/) installed.

> While `iex -S mix phx.server` is running


#### Creates per second

```
k6 run --vus 100 --duration 30s perf/create.js
```
#### Gets per second

```
k6 run --vus 100 --duration 30s perf/loadExisting.js
```



## Deployment

### Testing the deployment locally

To test this locally, if you have `docker` and `docker-compose` configured you can run:

```
mix release.test
```

This will build the dockerfile in release mode, and have everything needed to test it before going up to `fly.io`


### Fly.io deployment

To deploy the latest from your local environment, you will need to have `flyctl` [installed](https://fly.io/docs/hands-on/installing/) and configured

```
flyctl deploy
```