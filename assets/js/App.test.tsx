import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';
import App from './App';

test('renders created by text', () => {
  render(
    <MockedProvider>
      <App />
    </MockedProvider>
  );
  const linkElement = screen.getByText(/Created by Ed Bond/i);
  expect(linkElement).toBeInTheDocument();
});
