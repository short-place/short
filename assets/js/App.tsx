import * as React from 'react';
import Short from './components/Short';

function App() {
  return (
    <div className="min-h-full min-w-full p-10" style={{backgroundImage: "url('images/header.png')", backgroundRepeat: "no-repeat", backgroundSize: "cover" }}>
      <div className="h-full">
        <div className="w-full container mx-auto">
          <div className="w-full flex items-center justify-between">
            <a className="flex items-center text-indigo-400 no-underline hover:no-underline font-bold text-2xl lg:text-4xl" href="#">
            Short.<span className="bg-clip-text text-transparent bg-gradient-to-r from-blue-200 to-purple-500">Place</span>
            </a>

            <div className="flex w-1/2 justify-end content-center">
              <a className="inline-block text-blue-300 no-underline hover:text-pink-500 hover:text-underline text-center md:h-4 p-2 h-auto md:p-4 transform hover:scale-125 duration-300 ease-in-out" href="https://gitlab.com/short-place/short">
                <img className="mx-auto transform transition w-12 hover:scale-10 duration-700 ease-in-out hover:rotate-6" src="images/gitlab.svg" />
              </a>
              <a className="inline-block text-gray-300 no-underline hover:text-pink-500 hover:text-underline text-center md:h-4 p-2 h-auto md:p-4 transform hover:scale-125 duration-300 ease-in-out" href="https://www.linkedin.com/in/edwardbond/">
                <img className="mx-auto  transform transition w-10 hover:scale-10 duration-700 ease-in-out hover:rotate-6" src="images/linkedin.svg" />
              </a>            
            </div>
          </div>
        </div>

        <div className="container pt-24 md:pt-36 mx-auto flex flex-wrap flex-col md:flex-row items-center">

          <div className="flex flex-col w-full xl:w-3/5 justify-center lg:items-start overflow-y-hidden">
            <h1 className="my-4 text-3xl md:text-5xl text-white opacity-75 font-bold leading-tight text-center md:text-left">
              The
              <span className="bg-clip-text text-transparent bg-gradient-to-r from-blue-200 to-purple-500">
                &nbsp;Place&nbsp;
              </span>
              to 
              <span className="bg-clip-text text-transparent bg-gradient-to-r from-blue-200 to-purple-500">
                &nbsp;Short
              </span>
              en your urls!
            </h1>
            <p className="leading-normal text-base text-gray-100 md:text-2xl mb-8 text-center md:text-left">
              Ever want to have a shorter url to be able to type easier?
            </p>

            <Short />
          </div>

          <div className="w-full pt-16 pb-6 text-gray-100 text-sm text-center md:text-left fade-in">
            Created by Ed Bond
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
