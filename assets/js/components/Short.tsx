import * as React from 'react';
import { gql, useMutation } from '@apollo/client';


// Define mutation
export const SHORTEN_URL = gql`
  # Shorten a url mutation
  mutation ShortenUrl($url: String!){
    shortenUrl(url: $url){
      shortUrl
      targetUrl
    }
  }
`;


export default function (props) {
  let input;
  const [shortenUrl, { data, loading, error }] = useMutation(SHORTEN_URL);

  if (loading) return (
    <div
      className="bg-gray-900 opacity-75 w-full shadow-lg rounded-lg px-8 pt-6 pb-8 mb-4"
    >
      <label className="block text-blue-300 py-2 font-bold mb-2">
        Creating ...
      </label>
      <div
        className="shadow appearance-none border rounded w-full p-3 bg-gray-100 text-gray-700 leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
      >

      </div>

    </div>
  );
  if (error) return (
    <div
      className="bg-gray-900 opacity-75 w-full shadow-lg rounded-lg px-8 pt-6 pb-8 mb-4"
    >
      <label className="block text-blue-300 py-2 font-bold mb-2">
        Issue creating a short url for you.
        <br />
        {error.message}
        <br />
        Please make sure your url is in the format of http://google.com
      </label>

      <div className="flex items-center justify-between pt-4">
        <a
          href="/"
          className="bg-gradient-to-r from-purple-800 to-blue-200 hover:from-pink-500 hover:to-blue-500 text-white font-bold py-2 px-4 rounded focus:ring transform transition hover:scale-105 duration-300 ease-in-out"

        >
          Try another
        </a>
      </div>
    </div>
  )

  if (data) return (
    <div
      className="bg-gray-900 opacity-75 w-full shadow-lg rounded-lg px-8 pt-6 pb-8 mb-4"
    >
      <label className="block text-blue-300 py-2 font-bold mb-2">
        Here is your short url.
      </label>

      <div
        className="shadow appearance-none border rounded w-full p-3 bg-gray-100 text-gray-700 leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
      >
        {window.location.href}{data.shortenUrl.shortUrl}
      </div>

      <div className="flex items-center justify-between pt-4">
        <a
          href="/"
          className="bg-gradient-to-r from-purple-800 to-blue-200 hover:from-pink-500 hover:to-blue-500 text-white font-bold py-2 px-4 rounded focus:ring transform transition hover:scale-105 duration-300 ease-in-out"

        >
          Create Another
        </a>
      </div>
    </div>
  )

  return (
    <form
      className="bg-gray-900 opacity-75 w-full shadow-lg rounded-lg px-8 pt-6 pb-8 mb-4"
      onSubmit={e => {
        e.preventDefault();
        shortenUrl({ variables: { url: props.url || input.value } });
        input.value = '';
      }}
    >
      <div className="mb-4">
        <label className="block text-blue-300 py-2 font-bold mb-2">
          What URL would you like to shorten?
        </label>
        <input
          ref={node => {
            input = node;
          }}
          className="shadow appearance-none border rounded w-full p-3 text-gray-700 leading-tight focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
          id="url"
          type="text"
          placeholder="https://elixir-lang.org/getting-started/introduction.html#installation"
        />
      </div>

      <div className="flex items-center justify-between pt-4">
        <button
          className="bg-gradient-to-r from-purple-800 to-blue-200 hover:from-pink-500 hover:to-blue-500 text-white font-bold py-2 px-4 rounded focus:ring transform transition hover:scale-105 duration-300 ease-in-out"
          type="submit"
        >
          Shorten
        </button>
      </div>
    </form>
  );
}