import * as React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { render, screen, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import Short, { SHORTEN_URL } from './Short';

test('Presents the normal form', () => {
  render(
    <MockedProvider mocks={[]} addTypename={false}>
      <Short />
    </MockedProvider>,
  );
  const linkElement = screen.getByText(/What URL would you like to shorten?/i);
  expect(linkElement).toBeInTheDocument();
});


it('bad url', async () => {
  const mocks = [
    {
      request: {
        query: SHORTEN_URL,
        variables: {
          url: 'asdf',
        },
      },
      result: {
        data: {
          shortenUrl: null
        },
        errors: [
          {
            locations: [
              {
                column: 3,
                line: 2
              }
            ],
            message: "no_host",
            path: [
              "shortenUrl"
            ]
          }
        ]
      },
    },
  ];

  act(() => {
    render(
      <MockedProvider defaultOptions={{ mutate: { errorPolicy: 'all' } }} mocks={mocks} addTypename={false}>
        <Short url="asdf" />
      </MockedProvider>,
    );
  });


  const shortenButton = screen.getByText("Shorten");
  act(() => {
    shortenButton.click();
  });
  let displayText = screen.getByText(/Creating/i);
  expect(displayText).toBeInTheDocument();


  try {
    await act(async () => {
      await new Promise(resolve => setTimeout(resolve, 0)); // wait for response
    });
  } catch (error) {
    console.log(error)
  }




  displayText = screen.getByText(/no_host/i);
  expect(displayText).toBeInTheDocument();


});


it('good', async () => {
  const mocks = [
    {
      request: {
        query: SHORTEN_URL,
        variables: {
          url: 'https://localhost',
        },
      },
      result: {
        data: {
          shortenUrl: {
            shortUrl: "abcd",
            targetUrl: "https://localhost"
          }
        },
      },
    },
  ];

  act(() => {
    render(
      <MockedProvider defaultOptions={{ mutate: { errorPolicy: 'all' } }} mocks={mocks} addTypename={false}>
        <Short url="https://localhost" />
      </MockedProvider>,
    );
  });


  const shortenButton = screen.getByText("Shorten");
  act(() => {
    shortenButton.click();
  });
  let displayText = screen.getByText(/Creating/i);
  expect(displayText).toBeInTheDocument();


  try {
    await act(async () => {
      await new Promise(resolve => setTimeout(resolve, 0)); // wait for response
    });
  } catch (error) {
    console.log(error)
  }




  displayText = screen.getByText(/Here is your short url./i);
  expect(displayText).toBeInTheDocument();


});