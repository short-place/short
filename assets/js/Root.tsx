// This is the main entrypoint to create our singe page application
import "../css/app.css"
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';


import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql
} from "@apollo/client";

const client = new ApolloClient({
  uri: '/api',
  cache: new InMemoryCache()
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
);