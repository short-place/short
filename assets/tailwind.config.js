module.exports = {
    mode: "jit",
    purge: ["./js/**/*.tsx","./js/**/*.ts","./js/**/*.jsx","./js/**/*.js", "../lib/*_web/**/*.*ex"],
    theme: {
      extend: {},
    },
    variants: {
      extend: {},
    },
    plugins: [],
  };