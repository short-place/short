defmodule ShortWeb.GraphQLTest do
  use Short.DataCase
  use ExUnitProperties

  test "Example query" do
    response =
      """
      query {
        isValidUrl(url: "http://localhost")
      }
      """
      |> Absinthe.run(ShortWeb.Schema)

    assert {:ok, %{data: %{"isValidUrl" => true}}} == response
  end

  test "Example mutation" do
    {:ok, %{data: %{"shortenUrl" => response}}} =
      """
      mutation {
        shortenUrl(url: "http://localhost"){
          shortUrl
          targetUrl
        }
      }
      """
      |> Absinthe.run(ShortWeb.Schema)

    assert response["targetUrl"] == "http://localhost"
  end
end
