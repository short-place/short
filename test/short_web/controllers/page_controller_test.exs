defmodule ShortWeb.PageControllerTest do
  use ShortWeb.ConnCase

  # make sure we serve up a root content id
  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "root"
  end
end
