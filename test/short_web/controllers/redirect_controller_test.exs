defmodule ShortWeb.RedirectControllerTest do
  use ShortWeb.ConnCase
  alias Short.Urls

  # make sure we redirect when we have a redirect in our db
  test "get redirect", %{conn: conn} do
    {:ok, %{short_url: short}} = Urls.create("http://localhost")
    conn = get(conn, short)
    assert html_response(conn, 302) =~ "http://localhost"
  end

  test "default redirect", %{conn: conn} do
    conn = get(conn, "/asdf")
    assert html_response(conn, 302) =~ "<a href=\"/\">redirected</a>"
  end

  test "invalid redirect", %{conn: conn} do
    conn = get(conn, "/asdf/uwqiher")
    assert html_response(conn, 302) =~ "<a href=\"/\">redirected</a>"
  end
end
