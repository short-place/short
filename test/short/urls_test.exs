defmodule Short.UrlsTest do
  use Short.DataCase
  use ExUnitProperties

  alias Short.Urls

  describe "creating new short url" do
    test "insert a new url" do
      {:ok, _} = Urls.create("http://localhost")
    end

    test "collision check" do
      {:ok, val} = Urls.create("http://localhost")
      {:ok, second_val} = Urls.create("http://localhost", val.short_url)
      assert val.short_url != second_val.short_url
    end

    test "User can generate a new short url" do
      {:ok, _} = Urls.check_url("http://asdf.com")
      {:ok, _} = Urls.check_url("http://asdf.com/a")
      {:ok, _} = Urls.check_url("http://asdf.com/a?")
      {:ok, _} = Urls.check_url("http://asdf.com/a?c=3")
    end

    test "bad paths of new url creation" do
      {:error, _} = Urls.check_url(2)
      {:error, :no_host} = Urls.check_url("localhost")
      {:error, :no_host} = Urls.check_url("http://")
      {:error, :unsupported_scheme} = Urls.check_url("asdf://localhost")

      {:error, :user_info_provided} =
        Urls.check_url("https://do_not@allow:localhost/another_path")
    end

    test "property testing" do
      check all(size <- positive_integer()) do
        {:ok, short} = Urls.generate_short(size)
        assert String.length(short) == size
      end

      check all(
              size <- term(),
              not is_integer(size) or (is_integer(size) and size <= 0)
            ) do
        {:error, :invalid_length} = Urls.generate_short(size)
      end
    end

    test "generating the short part" do
      assert {:error, :invalid_length} == Urls.generate_short("asdfe")
    end
  end

  describe "urls" do
    alias Short.Urls.Url

    import Short.UrlsFixtures

    @invalid_attrs %{expiration: nil, short_url: nil, target_url: nil}

    test "list_urls/0 returns all urls" do
      url = url_fixture()
      assert Urls.list_urls() == [url]
    end

    test "get_url!/1 returns the url with given id" do
      url = url_fixture()
      assert Urls.get_url!(url.id) == url
    end

    test "update_url/2 with valid data updates the url" do
      url = url_fixture()

      update_attrs = %{
        expiration: ~N[2021-11-06 07:37:00],
        short_url: "some updated short_url",
        target_url: "some updated target_url"
      }

      assert {:ok, %Url{} = url} = Urls.update_url(url, update_attrs)
      assert url.expiration == ~N[2021-11-06 07:37:00]
      assert url.short_url == "some updated short_url"
      assert url.target_url == "some updated target_url"
    end

    test "update_url/2 with invalid data returns error changeset" do
      url = url_fixture()
      assert {:error, %Ecto.Changeset{}} = Urls.update_url(url, @invalid_attrs)
      assert url == Urls.get_url!(url.id)
    end

    test "delete_url/1 deletes the url" do
      url = url_fixture()
      assert {:ok, %Url{}} = Urls.delete_url(url)
      assert_raise Ecto.NoResultsError, fn -> Urls.get_url!(url.id) end
    end

    test "change_url/1 returns a url changeset" do
      url = url_fixture()
      assert %Ecto.Changeset{} = Urls.change_url(url)
    end
  end
end
