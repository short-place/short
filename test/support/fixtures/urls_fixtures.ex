defmodule Short.UrlsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Short.Urls` context.
  """

  @doc """
  Generate a url.
  """
  def url_fixture(attrs \\ %{}) do
    {:ok, url} =
      attrs
      |> Enum.into(%{
        expiration: ~N[2021-11-05 07:37:00],
        short_url: "some short_url",
        target_url: "some target_url"
      })
      |> Short.Urls.create_url()

    url
  end
end
