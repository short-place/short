defmodule Short.MixProject do
  use Mix.Project

  def project do
    [
      app: :short,
      version: "0.1.0",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Short.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.6.2"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.6"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.16.0"},
      {:stream_data, "~> 0.5", only: :test},
      {:floki, ">= 0.30.0", only: :test},
      {:phoenix_live_dashboard, "~> 0.5"},
      {:esbuild, "~> 0.2", runtime: Mix.env() == :dev},
      {:swoosh, "~> 1.3"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.18"},
      {:jason, "~> 1.2"},
      {:absinthe_phoenix, "~> 2.0.0"},
      {:cachex, "~> 3.4"},
      {:plug_cowboy, "~> 2.5"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: [
        # allow this command to fail if docker-compose not used
        "cmd docker-compose up -d || true",
        "cmd echo sleeping for postgres",
        "cmd sleep 10",
        "deps.get",
        "ecto.setup",
        "node.install"
      ],
      "node.install": ["cmd cd assets && npm install"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test.all": ["cmd MIX_ENV=test mix test --cover", "test.ui"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "test.ui": ["cmd cd assets && npm test"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"],
      "release.test": ["cmd docker-compose -f docker-compose.release.yml up --build"],
      "release.test.down": ["cmd docker-compose -f docker-compose.release.yml down"]
    ]
  end
end
