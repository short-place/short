defmodule Short.Repo.Migrations.CreateUrls do
  use Ecto.Migration

  def change do
    create table(:urls) do
      add :short_url, :string
      add :target_url, :string
      add :expiration, :naive_datetime

      timestamps()
    end

    create unique_index(:urls, [:short_url])
  end
end
