import http from 'k6/http';



export function setup() {
    const url = 'http://localhost:4000/api';
    const payload = `
        mutation{
            shortenUrl(url: "https://localhost"){
            shortUrl
            }
        }
    `;
    var short = JSON.parse(http.post(url, payload).body).data.shortenUrl.shortUrl;
    var urlToHit = "http://localhost:4000/" + short;
    return urlToHit;
}


export default function (urlToHit) {
    http.get(urlToHit, { redirects: 0 });
}