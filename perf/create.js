import http from 'k6/http';

export default function () {
    const url = 'http://localhost:4000/api';
    const payload = `
        mutation{
            shortenUrl(url: "https://localhost"){
            shortUrl
            targetUrl
            }
        }
    `;
    http.post(url, payload);
}