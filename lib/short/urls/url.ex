defmodule Short.Urls.Url do
  @moduledoc """
  This is our ecto schema of our short urls.
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "urls" do
    field :expiration, :naive_datetime
    field :short_url, :string
    field :target_url, :string

    timestamps()
  end

  @doc false
  def changeset(url, attrs) do
    url
    |> cast(attrs, [:short_url, :target_url, :expiration])
    |> validate_required([:short_url, :target_url])
  end
end
