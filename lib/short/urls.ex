defmodule Short.Urls do
  @moduledoc """
  The Urls context.
  """

  import Ecto.Query, warn: false
  alias Short.Repo

  alias Short.Urls.Url

  @start_length 5
  @random_string_set '0123456789abcdefhijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

  @doc """
  Check a target url for validity

  Break out the validation and the business logic.
  """
  def check_url("" <> target_url) do
    # pattern match first for speed, then guard clauses for input validation
    case URI.parse(target_url) do
      %{userinfo: "" <> _} -> {:error, :user_info_provided}
      %{host: nil} -> {:error, :no_host}
      %{host: ""} -> {:error, :no_host}
      %{scheme: scheme} when scheme not in ["https", "http"] -> {:error, :unsupported_scheme}
      _uri -> {:ok, :good_url}
    end
  end

  def check_url(_), do: {:error, :invalid_format}

  @doc """
  Create a new url entry in our database based upon a checked url.
  """
  def create(target_url, preferred_url \\ nil) do
    with {:url_check, {:ok, _}} <- {:url_check, check_url(target_url)},
         {:ecto_entry, {:ok, val}} <- {:ecto_entry, create_url_in_db(target_url, preferred_url)} do
      {:ok, val}
    else
      {:url_check, err} -> err
      err -> err
    end
  end

  # Create the url in the database with the preferred url if it is there
  # if it isn't available we generate another unique url via recursion
  defp create_url_in_db(target_url, preferred_url, length_check \\ @start_length) do
    case generate_short(length_check) do
      {:ok, short} ->
        try do
          %Url{}
          |> Url.changeset(%{
            short_url: preferred_url || short,
            target_url: target_url
          })
          |> Repo.insert()
        rescue
          Ecto.ConstraintError -> create_url_in_db(target_url, nil, length_check + 1)
        end

      val ->
        val
    end
  end

  @doc """
  Generate a random string based upon a length
  """
  def generate_short(length)
      when is_integer(length) and
             length > 0 do
    string = for _ <- 1..length, into: "", do: <<Enum.random(@random_string_set)>>
    {:ok, string}
  end

  def generate_short(_), do: {:error, :invalid_length}

  @doc """
  Returns the list of urls.

  ## Examples

      iex> list_urls()
      [%Url{}, ...]

  """
  def list_urls do
    Repo.all(Url)
  end

  @doc """
  Gets a single url.

  Raises `Ecto.NoResultsError` if the Url does not exist.

  ## Examples

      iex> get_url!(123)
      %Url{}

      iex> get_url!(456)
      ** (Ecto.NoResultsError)

  """
  def get_url!(id), do: Repo.get!(Url, id)

  @doc """
  Get urn by the short url.any()
    Check cache
    Hydrate cache if need be
  """
  def get_url_by_short_url(url) do
    # Lookup by cache first, otherwise populate
    case Cachex.get(:short_url, url) do
      {:ok, nil} ->
        case Repo.get_by(Url, short_url: url) do
          nil ->
            nil

          %Url{} = short_url ->
            Cachex.put(:short_url, url, short_url)
            short_url
        end

      {:ok, val} ->
        val
    end
  end

  @doc """
  Creates a url.

  ## Examples

      iex> create_url(%{field: value})
      {:ok, %Url{}}

      iex> create_url(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_url(attrs \\ %{}) do
    %Url{}
    |> Url.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a url.

  ## Examples

      iex> update_url(url, %{field: new_value})
      {:ok, %Url{}}

      iex> update_url(url, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_url(%Url{} = url, attrs) do
    url
    |> Url.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a url.

  ## Examples

      iex> delete_url(url)
      {:ok, %Url{}}

      iex> delete_url(url)
      {:error, %Ecto.Changeset{}}

  """
  def delete_url(%Url{} = url) do
    Repo.delete(url)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking url changes.

  ## Examples

      iex> change_url(url)
      %Ecto.Changeset{data: %Url{}}

  """
  def change_url(%Url{} = url, attrs \\ %{}) do
    Url.changeset(url, attrs)
  end
end
