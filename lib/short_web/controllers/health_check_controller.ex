defmodule ShortWeb.HealthCheckController do
  @moduledoc """
    The purpose of this is to respond to health checks
  """
  use ShortWeb, :controller
  alias Short.Repo
  import Ecto.Adapters.SQL

  def index(conn, _) do
    try do
      query(Repo, "SELECT 1")
    rescue
      e in RuntimeError -> e
    end
    |> case do
      {:ok, _} ->
        text(conn, "healthy")

      _ ->
        conn
        |> put_status(500)
        |> text("unhealthy")
    end
  end
end
