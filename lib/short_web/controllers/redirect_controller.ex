defmodule ShortWeb.RedirectController do
  @moduledoc """
  This is our Redirect controller

  The purpose of this is to lookup a short url and redirect based upon the input
  """
  use ShortWeb, :controller
  alias Short.Urls

  @doc """
  If we can find the url we redirect, if we can't redirect to the main index.
  """
  def index(conn, %{"short_url" => [short_url]}) do
    case Urls.get_url_by_short_url(short_url) do
      nil ->
        redirect(conn, to: "/")

      %{target_url: target_url} ->
        redirect(conn, external: target_url)
    end
  end

  def index(conn, _) do
    redirect(conn, to: "/")
  end
end
