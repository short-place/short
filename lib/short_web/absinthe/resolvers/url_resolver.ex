defmodule ShortWeb.UrlResolver do
  @moduledoc """
  Our absinthe resolvers to bridge the gap between the web and our context
  """
  alias Short.Urls
  alias Short.Urls.Url

  @doc """
  Pass through to our create url
  """

  def create_url(_root, %{url: url}, _info) do
    case Urls.create(url) do
      {:ok, val} -> {:ok, translate_url_for_ui(val)}
      val -> val
    end
  end

  defp translate_url_for_ui(%Url{expiration: nil} = url), do: url

  @doc """
  Absinthe resolver to
  """
  def valid_url?(_root, %{url: url}, _info) do
    case Urls.check_url(url) do
      {:ok, _} -> {:ok, true}
      val -> val
    end
  end
end
