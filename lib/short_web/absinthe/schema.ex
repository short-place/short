defmodule ShortWeb.Schema do
  @moduledoc """
  This is our schema definition for GraphQL
  """
  use Absinthe.Schema

  alias ShortWeb.UrlResolver

  object :url do
    field :id, non_null(:id)
    field :short_url, non_null(:string)
    field :target_url, non_null(:string)
    field :expiration, :integer
  end

  query do
    field :is_valid_url, :boolean do
      arg(:url, non_null(:string))
      resolve(&UrlResolver.valid_url?/3)
    end
  end

  mutation do
    @desc "Create a new short url"
    field :shorten_url, :url do
      arg(:url, non_null(:string))
      arg(:expire_from_now_in_seconds, :integer)
      resolve(&UrlResolver.create_url/3)
    end
  end
end
